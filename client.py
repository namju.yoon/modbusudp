import socket
import sys
import struct
from ctypes import c_int16

HOST, PORT = "localhost", 5000
BUFFERSIZE = 1024 

speed = 100  # 1 = 1000 Hz, 10 = 100 Hz, ...
filt = 10   # don't pre-filter data
zero = 9

udpSocketClient = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    msg = input('서버에 전달할 숫자를 입력하세요: ')
    if msg == 'q':
        break

    my_list = msg.split()
    my_list = list(map(int, my_list))
    bytesToSend = struct.pack("<"+"h"*len(my_list),*my_list)
    print(my_list, bytesToSend)

    # bytesToSend = struct.pack('>HHHh', speed, filt, zero, num)

    udpSocketClient.sendto(bytesToSend, (HOST, PORT))
    # print("#"*5, "Sent:     {}".format(bytesToSend))

    received = udpSocketClient.recvfrom(BUFFERSIZE)
    data = received[0] 
    address = received[1]

    # speed, filt, zero, num = (
    #     struct.unpack('<hhh', message)
    # )

    receiveData = list()
    for i in range(0, 3):
        receiveData.append(c_int16(data[i * 2] + (data[i * 2 + 1] << 8)).value )

    msgFromServer = f"from SERVER MESSAGE : {receiveData}"
    print(data, msgFromServer, "\n\n")

    