#include <stdio.h>

int main(){
	short int endian = 0x1234;
	char *ptr = (char*)&endian;

	if (*ptr == 0x12)
		printf("Big Endia Architecture");
	else if (*ptr == 0x34)
		printf("Little Endia Architecture");
	else
		printf("Unknown");
	return 0;
}