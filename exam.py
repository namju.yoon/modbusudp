
## Big
record = struct.pack('<10sHHb', b'raymond   ', 4568, 264, 8)
# record =  b'raymond   \xd8\x11\x08\x01\x08'

name, serialnum, school, gradelevel = unpack('<10sHHb', record)


from collections import namedtuple
Student = namedtuple('Student', 'name serialnum school gradelevel')
Student._make(struct.unpack('<10sHHb', record))
# Student(name=b'raymond   ', serialnum=4568, school=264, gradelevel=8)


## Little
record = struct.pack('>10sHHb', b'raymond   ', 4568, 264, 8)
# record =  b'raymond   \x11\xd8\x01\x08\x08'

name, serialnum, school, gradelevel = unpack('<10sHHb', record)


from collections import namedtuple
Student = namedtuple('Student', 'name serialnum school gradelevel')
Student._make(struct.unpack('>10sHHb', record))
# Student(name=b'raymond   ', serialnum=4568, school=264, gradelevel=8)

##
struct.pack('<ci', b'*', 0x12131415)
struct.pack('>ci', b'*', 0x12131415)

struct.pack('<ic', 0x12131415, b'*')
struct.pack('>ic', 0x12131415, b'*')


struct.calcsize('<ic')
struct.calcsize('>ic')

struct.pack('<llh0l', 1, 2, 3)
struct.pack('>llh0l', 1, 2, 3)

struct.calcsize('<llh0l')
struct.calcsize('>llh0l')

struct.pack('>3f', 1.5, 2.5, 3.5)
struct.calcsize('>3f')


header = (170, 0, 50, 3)
speed = 1  # 1 = 1000 Hz, 10 = 100 Hz, ...
filt = 0   # don't pre-filter data
zero = 255
checksum = sum(header) + speed + filt + zero
payload = (*header, speed, filt, zero, *checksum.to_bytes(2, 'big', signed=False))
opt_ser.write(bytes(payload))



class Smartphone:
    """
    Smartphone Class
    """
    # 클래스 변수
    smartphone_count = 0
    def __init__(self, brand, infomations):
        self._brand = brand
        self._infomations = infomations
        Smartphone.smartphone_count += 1
    def __str__(self):
        return f'str : {self._brand} - {self._infomations}'
    def __repr__(self):
        return f'repr : {self._brand} - {self._infomations}'
    def infomation(self):
        print(f"Current Id : {id(self)}")
        print(f"Smartphone Price : {self._brand} {self._infomations.get('price')}")


Smartphone1 = Smartphone('Iphone', {'color' : 'White', 'price': 10000})
Smartphone2 = Smartphone('Galaxy', {'color' : 'Black', 'price': 8000})


class Smartphone:
    def __init__(self, brand, price):
        self._brand = brand
        self._price = price
    @property
    def price(self):
        return self._price
    @price.setter
    def price(self, price):
        if price < 0:
            raise ValueError("Price below 0 is not possible")
        print(f"변경 전 가격 : {self._price}")
        self._price = price
        print(f"변경 후 가격 : {self._price}")

Smartphone1 = Smartphone("Iphone", 1000)
