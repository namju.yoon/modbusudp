class MessageStructure(object):
	"""
	"MSG Header: (6 Bytes)"
		"MSG ID: (4 Bytes)"								
			"Source ID : (1 Byte)"		
			"Destination ID : (1 Byte)"		
			"Code : (1 Byte)"	--> 송신 메시지 코드
								--> 수신 메시지 코드
			"Ack 요청/결과 : (1 Byte)"	--> "0xFF : Ack 미요청
										--> 0x01 : Ack 요청함"
		"Data Length : (2 Bytes)"
				Data 크기(바이트단위)	
	"""
	def __init__(self):
		self.source_id = None		# 1 Byte
		self.destination_id = None	# 1 Byte
		self.code = None 			# Send / Receive
		self.ack = None 			# 0xFF : Ack 미요청 || 0x01 : Ack 요청함
		self.data_length = None 	# Data Length :: (2 Bytes)

		self.data = None 			# (Max 1016 Bytes)
		self.checksum = None		# 2 Byte

	

