# import SocketServer, threading, time

# class ThreadedUDPRequestHandler(SocketServer.BaseRequestHandler):

#     def handle(self):
#         data = self.request[0].strip()
#         socket = self.request[1]
#         current_thread = threading.current_thread()
#         print("{}: client: {}, wrote: {}".format(current_thread.name, self.client_address, data))
#         socket.sendto(data.upper(), self.client_address)

# class ThreadedUDPServer(SocketServer.ThreadingMixIn, SocketServer.UDPServer):
#     pass

# if __name__ == "__main__":
#     HOST, PORT = "0.0.0.0", 8888

#     server = ThreadedUDPServer((HOST, PORT), ThreadedUDPRequestHandler)

#     server_thread = threading.Thread(target=server.serve_forever)
#     server_thread.daemon = True

#     try:
#         server_thread.start()
#         print("Server started at {} port {}".format(HOST, PORT))
#         while True: time.sleep(100)
#     except (KeyboardInterrupt, SystemExit):
#         server.shutdown()
#         server.server_close()
#         exit()
#         

# struct.pack(">"+"H"*len(data),*data)

import socket 
import struct
from ctypes import c_int16

localIP = "127.0.0.1" 
localPort = 5000 
BUFFERSIZE = 1024 
msgFromServer = "Hello UDP Client" 
bytesToSend = str.encode(msgFromServer) 

# 데이터그램 소켓을 생성 
udpServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM) 
# 주소와 IP로 Bind 

udpServerSocket.bind((localIP, localPort)) 
print("#"*5, "UDP server up and listening", "#"*5) 

# 들어오는 데이터그램 Listen 
while(True):
    bytesAddressPair = udpServerSocket.recvfrom(BUFFERSIZE)
    data = bytesAddressPair[0] 
    address = bytesAddressPair[1]
    

    # speed, filt, zero = (
    #     struct.unpack('<hhh', message)
    # )

    receiveData = list()
    for i in range(0, 3):
        receiveData.append(c_int16(data[i * 2] + (data[i * 2 + 1] << 8)).value )

    print(data, len(data), receiveData)
    msgFromServer = f"from client MESSAGE : {receiveData[0]} :: {receiveData[1]} :: {receiveData[2]} :: FROM SERVER"
    bytesToSend = struct.pack("<"+"h"*len(receiveData),*receiveData)
    print(bytesToSend, msgFromServer, "\n\n")

    # bytesToSend = str.encode(bytesToSend) 
    # print("After Encoding :: ", bytesToSend, "\n\n")

    # Sending a reply to client 
    udpServerSocket.sendto(bytesToSend, address)

